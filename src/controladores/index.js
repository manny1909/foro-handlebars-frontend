const axios =require('axios')
const ctrl={
    getPosts:async()=>{
        const posts=await axios.get('http://localhost:3000/api/posts')
        return posts.data;
    },
    addUser:async({nombre,email,clave})=>{
        await axios.post('http://localhost:3000/api/user/add',{nombre,email,password:clave})
        return true
    },
    auth:async({email,password})=>{
     const res= await axios.post('http://localhost:3000/api/auth',{email,password})
     const obj=res.data
     return obj;
    },
    getComments:async(id)=>{
        const response =await axios.get(`http://localhost:3000/api/comments/${id}`)
        const obj=response.data
        return obj
    }
}
module.exports=ctrl;