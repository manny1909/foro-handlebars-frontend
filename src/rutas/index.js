const { Router }=require('express')
const rutas=Router()
const ctrl=require('../controladores/index')
rutas.route('/')
    .get(async(req,res)=>{
        const posts=await ctrl.getPosts()
        res.render('partials/home',{posts})
    })
rutas.route('/registrarse')
    .get((req,res)=>{
        res.render('partials/registrarse')
    })
    .post(async(req,res)=>{
        const {nombre,email,clave}=req.body
        const estado=await ctrl.addUser({nombre,email,clave})
        estado && console.log('usuario creado satisfactoriamente');
        res.redirect('/')
    })
rutas.post('/autenticacion',async(req,res)=>{
    const {email,clave}=req.body
    const response=await ctrl.auth({email,password:clave})
    response && console.log(`usuario loggeado con token: ${response.token}`);
    res.redirect('/')
})
module.exports=rutas