const express =require('express')
const app =express()
const morgan=require('morgan')
const exphbs=require('express-handlebars')
const cors=require('cors')
const path=require('path')

app.set('port', process.env.PORT || '4000')
app.set('views',path.join(__dirname,'/vistas'))
app.engine(
    ".hbs",
    exphbs({
      defaultLayout: "main",
      layoutsDir: path.join(app.get("views"), "/layouts"),
      partialsDir: path.join(app.get("views"), "/partials"),
      extname: ".hbs",
    })
  );
  app.set("view engine", ".hbs");
app.use(express.static(path.join(__dirname,'publico')))
app.use(express.json())
app.use(express.urlencoded({extended:false}))
//rutas
app.use('/',require('./rutas/index'))
module.exports=app